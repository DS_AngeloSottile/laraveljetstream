require('./bootstrap');
require('./alpineTest');

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();
