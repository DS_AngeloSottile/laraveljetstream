document.addEventListener('alpine:init', () =>
{
    Alpine.data('dropdown2', (initialOpenState) => ({
        open: initialOpenState,

        toggle()
        {
            this.open = ! this.open
        },
    }));

    Alpine.data('dropdown', () => ({
        init() {
            console.log('I will get evaluated when initializing each "dropdown" component.')
        },
        open: false,

        trigger:
        {
            init:  console.log("sono nel trigger") ,
            ['@click']()
            {
                console.log("sono nel trigger")
                this.open = ! this.open
            },
        },

        dialogue: {
            ['x-show']() {
                return this.open
            },
        },
    }))
})
