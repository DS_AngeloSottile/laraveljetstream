<x-app-layout>
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="mx-auto max-w-7xl sm:px-6 lg:px-8">
            <div class="overflow-hidden bg-white shadow-xl sm:rounded-lg">

                <div class="p-6 text-yellow-600 bg-indigo-500"> test</div>
                <div class="p-6 text-red-500 bg-green-300 "> test</div>

                <div class="flex justify-around w-full">
                    <div class="w-12 h-12 bg-red-700"></div>
                    <div class="w-12 h-12 bg-red-700"></div>
                    <div class="w-12 h-12 bg-red-700"></div>
                </div>

                <x-jet-welcome />
            </div>
        </div>
    </div>
</x-app-layout>
